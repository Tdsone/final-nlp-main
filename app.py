import os
import json
from flask import Flask, jsonify, request, send_from_directory

from text2KG import Text2KG
from score_calculator import KG2Score

import score_calculator
import db

app = Flask(__name__)

# Instantiating Text2KG once, so that spacy is only loaded once
text2KG = Text2KG()
kg2score = KG2Score()


@app.route("/")
def hello():
    return send_from_directory("public", "index.html")


@app.route("/start-exam/<topic>")
def ask_question(topic):
    res = db.get_all_exam_questions(topic)
    if res["has_errored"]:
        return jsonify({'error': res["error"]}), 404
    return jsonify(res)


""" 
Compares a set of questions and grades them

Param: array of objects: [{questionID, answer}, ...]
Return: object of the form {final_grade, question_grades: [{questionID, grade}]}
"""


@app.route("/grade-exam", methods=["POST"])
def score_answer():

    data = request.json
    grades = []
    try:
        questions = data["questions"]
        answers = data["answers"]

        if len(questions) != len(answers):
            return jsonify({'has_errored': 'True', "error": "Number of answers does not match number of question"}), 400
        for i in range(len(questions)):
            answer = answers[i]
            question = questions[i]
            # testing only
            #grade = 1
            print("\tI'm computing the {}th question.".format(i+1))
            grade = score_calculator.grade_answer(
                question, answer, text2KG, kg2score)
            grades.append(grade)

        # final grade is just the average of all grades
        if len(grades) > 0:
            final_grade = sum(grades) / len(grades)
        else:
            final_grade = -1
        return jsonify({
            "data": {
                "final_grade": final_grade,
                "grades": grades
            },
            "has_errored": False
        })
    except Exception as e:
        return jsonify({'has_errored': 'True', "error": e}), 400


# From here resource based APIs begin


@app.route("/questions", methods=["POST"])
def create_question():
    questions = request.json
    created_questions = []
    for question in questions:
        created = db.create_question(question)
        if not created["has_errored"]:
            created_questions.append(created["data"])
        else:
            return jsonify({
                "error": created["error"],
                "has_errored": created["has_errored"]
            })
    return jsonify({
        "created_questions": created_questions
    })


if __name__ == "__main__":
    app.debug = True
    app.run(host="0.0.0.0")
