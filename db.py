import os
from data import questions_table, DBEntry

# finds all questions with a given topic
# param: topic (string)
# return [{questionID, question}, ...]


def get_all_exam_questions(topic):
    try:
        response = [entry.question
                    for entry in questions_table if entry.topic == topic]
        return {"has_errored": False, "data": response}
    except Exception as e:
        print("An error ocurred in get_all_exam_questions")
        print(e)
        return {"has_errored": True, "error": e}


def get_solution(question):
    try:
        response = next(
            entry.solution for entry in questions_table if question == entry.question)
        return {"has_errored": False, "data": response}
    except Exception as e:
        print("An error ocurred in get_solution_by_ID")
        print(e)
        return {"has_errored": True, "error": e}


def create_question(data):
    try:
        question = data["question"]
        solution = data["solution"]
        topic = data["topic"]
        if not question or (not solution) or (not topic):
            print("An error occurred in create_question: a property is missing")
            return {"has_errored": True, "error": 'An error occurred in create_question: a property is missing'}

        questions_table.append(DBEntry(question, solution, topic))

        print("A new question was successfully inserted")
        return {"has_errored": False, "data": {'question': question,
                                               'solution': solution,
                                               'topic': topic}}
    except Exception as e:
        print("An error occurred in create_question")
        print(e)
        return {"has_errored": True, "error": e}
