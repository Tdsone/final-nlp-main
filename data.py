class DBEntry:

    def __init__(self, question, solution, topic):
        self.question = question
        self.solution = solution
        self.topic = topic

    def toDict(self):
        return {
            "question": self.question,
            "solution": self.solution,
            "topic": self.topic
        }


questions_table = [
    DBEntry("Why is the earth round?",
            "The earth is round because it was a liquid turing lava ball and gravitational forces did the rest", "geography"),
    DBEntry("How did the Alps come into existence", "No idea", "geography"),
    DBEntry("Why do continents exist?",
            "Earth was once one big continent which broke up.", "geography"),
    DBEntry("What is climate change?",
            "Something we should stop.", "geography"),
    DBEntry("Do you like geography questions?",
            "There is no good answer to this question.", "geography"),
    DBEntry("Why did Napoleon conquer Germany?",
            "Because he could.", "history"),
    DBEntry("What is a Neural Network?","A neural Network is a tool from artificial intelligence. Neural networks consist out of neurons. Neurons are connected with others. Neural Networks are build out of multiple layers. A node is activated if its threshold is over a certain value.","intelligence"),    
    DBEntry("What is NLP?","NLP stands Natural Language Processing. It is a field of AI. NLP focuses on creating language with the help of machine learning.","intelligence"),
    DBEntry("What is a Knowlege Graph?","Knowledge graphs are a type of ontologies. They consist out of nodes and edges. The nodes stand for concepts. Edges hold relations between the concepts.","intelligence"),      

]
